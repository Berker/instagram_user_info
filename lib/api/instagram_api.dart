import 'package:instagram_public_api/instagram_public_api.dart';

class InstagramApi {
  static Future<InstaProfileData> fetchUserData(String userName) async {
    FlutterInsta insta = FlutterInsta();

    InstaProfileData userData = await insta.getProfileData(userName);
    return userData;
  }
}
