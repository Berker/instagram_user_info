import 'package:flutter/material.dart';
import 'package:instagram_user_info/api/instagram_api.dart';
import 'package:instagram_user_info/pages/user_info_page.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:instagram_user_info/widgets/appbar_widget.dart';

class UserSearchPage extends StatefulWidget {
  const UserSearchPage({Key? key}) : super(key: key);

  @override
  _UserSearchPageState createState() => _UserSearchPageState();
}

class _UserSearchPageState extends State<UserSearchPage> {
  final _form = GlobalKey<FormState>();
  final _submitFocusNode = FocusNode();
  String _username = "";
  bool _isLoading = false;

  bool _saveForm() {
    final isValid = _form.currentState!.validate();
    if (isValid) {
      _form.currentState!.save();
      return true;
    }
    return false;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const AppBarWidget(
        backgroundColor: Colors.transparent,
      ),
      body: SafeArea(
        child: Stack(children: [
          Center(
            child: Container(
              constraints: const BoxConstraints.expand(
                width: 350,
                height: 380,
              ),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(5),
                  gradient: const LinearGradient(
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter,
                      colors: [Colors.red, Colors.yellow])),
              child: Stack(
                children: [
                  Positioned(
                    bottom: 70, left: 10, height: 300, //width: 200,
                    child: RotatedBox(
                      quarterTurns: 3,
                      child: Form(
                        key: _form,
                        child: TextFormField(
                          onFieldSubmitted: (_) => FocusScope.of(context)
                              .requestFocus(_submitFocusNode),
                          decoration: const InputDecoration(
                            labelText: "Username",
                            border: OutlineInputBorder(),
                          ),
                          validator: (value) {
                            if (value!.isEmpty) {
                              return "Please fill out this field";
                            }
                            return null;
                          },
                          onSaved: (value) {
                            _username = value.toString();
                          },
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    bottom: 10,
                    left: 15,
                    child: SizedBox(
                      height: 50,
                      width: 50,
                      child: SvgPicture.network(
                          "https://upload.wikimedia.org/wikipedia/commons/3/3e/Instagram_simple_icon.svg"),
                    ),
                  ),
                  Positioned(
                    bottom: 10,
                    right: 10,
                    child: OutlinedButton.icon(
                      onPressed: () async {
                        if (_saveForm()) {
                          setState(() {
                            _isLoading = true;
                          });
                          await InstagramApi.fetchUserData(_username).then(
                              (value) {
                            setState(() {
                              _isLoading = false;
                            });
                            if (value.isPrivate!) {
                              const snackBar = SnackBar(
                                content: Text("User is private!"),
                              );
                              ScaffoldMessenger.of(context)
                                  .showSnackBar(snackBar);
                              return;
                            }
                            Navigator.of(context).pushNamed(
                                UserInfoPage.routeName,
                                arguments: value);
                          }, onError: (error) {
                            setState(() {
                              _isLoading = false;
                            });
                            if (error.runtimeType.toString() ==
                                "NoSuchMethodError") {
                              const snackBar = SnackBar(
                                content: Text("User not found!"),
                              );
                              ScaffoldMessenger.of(context)
                                  .showSnackBar(snackBar);
                              return;
                            }
                            final snackBar = SnackBar(
                              content: Text(error.toString()),
                            );
                            ScaffoldMessenger.of(context)
                                .showSnackBar(snackBar);
                          });
                        }
                      },
                      focusNode: _submitFocusNode,
                      icon: const Icon(Icons.send),
                      label: const Text("Submit"),
                      style: ButtonStyle(
                        padding: MaterialStateProperty.all(
                          const EdgeInsets.symmetric(
                              horizontal: 95, vertical: 15),
                        ),
                      ),
                    ),
                  ),
                  const Positioned(
                    top: 10,
                    right: 50,
                    width: 200,
                    height: 200,
                    child: Text(
                      "Please enter a public instagram username and hit submit button to display its public data",
                      style: TextStyle(
                        fontSize: 16,
                        fontStyle: FontStyle.italic,
                      ),
                    ),
                  ),

                  /* const Padding(
                          padding: EdgeInsets.all(10.0),
                          child: Text(
                            "Please enter a public instagram username to display the public data",
                            style: TextStyle(
                              fontSize: 16,
                              fontStyle: FontStyle.italic,
                            ),
                          ),
                        ), */
                ],
              ),
            ),
          ),
          if (_isLoading)
            const Center(
              child: CircularProgressIndicator(),
            ),
        ]),
      ),
    );
  }
}
