import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:instagram_public_api/instagram_public_api.dart';
import 'package:instagram_user_info/widgets/appbar_widget.dart';
import 'package:instagram_user_info/widgets/circle_image.dart';

class UserInfoPage extends StatelessWidget {
  const UserInfoPage({Key? key}) : super(key: key);

  static const routeName = "/user-info-page";

  @override
  Widget build(BuildContext context) {
    final profile =
        ModalRoute.of(context)!.settings.arguments as InstaProfileData;
    bool _isVerified = profile.isVerified as bool;
    String _bio = profile.bio!.isNotEmpty
        ? profile.bio as String
        : "No bio information found for this profile.";
    return Scaffold(
      appBar: const AppBarWidget(
        backgroundColor: Colors.red,
      ),
      body: SafeArea(
        child: Center(
          child: Container(
            decoration: const BoxDecoration(
                gradient: LinearGradient(
                    colors: [Colors.red, Colors.yellow],
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter)),
            child: Column(
              children: [
                CircleImage(
                  imageProvider: NetworkImage(profile.profilePicURL.toString()),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        profile.username.toString(),
                      ),
                      if (_isVerified)
                        const Padding(
                          padding: EdgeInsets.all(8.0),
                          child: Icon(
                            Icons.verified_outlined,
                            color: Colors.blue,
                          ),
                        ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Column(
                        children: [
                          Row(
                            children: const [
                              Padding(
                                padding: EdgeInsets.only(right: 8.0),
                                child: Icon(Icons.person),
                              ),
                              Text(
                                "Followers",
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                            ],
                          ),
                          Text(
                            profile.followers.toString(),
                          ),
                        ],
                      ),
                      const SizedBox(
                          height: 40,
                          child: VerticalDivider(
                            color: Colors.black,
                          )),
                      Column(
                        children: [
                          Row(
                            children: const [
                              Padding(
                                padding: EdgeInsets.only(right: 8.0),
                                child: Icon(Icons.favorite_border_outlined),
                              ),
                              Text(
                                "Following",
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                            ],
                          ),
                          Text(
                            profile.following.toString(),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 8.0),
                  child: Column(
                    children: [
                      const Text(
                        "Bio",
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                      const Divider(
                        indent: 50,
                        endIndent: 50,
                        color: Colors.black,
                      ),
                      Padding(
                        padding: const EdgeInsets.only(
                          top: 8.0,
                          left: 20,
                          right: 20,
                        ),
                        child: Text(
                          _bio,
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
