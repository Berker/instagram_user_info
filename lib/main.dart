import 'package:flutter/material.dart';
import 'package:instagram_user_info/pages/user_info_page.dart';
import 'package:instagram_user_info/pages/user_search_page.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Instagram User Info',
      theme: ThemeData(
        primarySwatch: Colors.cyan,
      ),
      home: UserSearchPage(),
      routes: {
        UserInfoPage.routeName: (ctx) => UserInfoPage(),
      },
    );
  }
}
